﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayVideo : MonoBehaviour
{
    public GameObject video_player;
    public int stop_time;
    void Start()
    {
        video_player.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.transform.gameObject)
                {
                    video_player.SetActive(true);
                    Destroy(video_player, stop_time);
                  
                }
            }
        }
    }
}
