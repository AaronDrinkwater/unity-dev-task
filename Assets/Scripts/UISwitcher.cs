﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class UISwitcher : MonoBehaviour
{
    public GameObject Image;
    public GameObject Panel2Disapear;
    public GameObject Panel2Appear;
    void Start()
    {
        
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.transform.gameObject && Image.gameObject)
                {
                    Panel2Disapear.SetActive(false);
                    Panel2Appear.SetActive(true);
                }
            }
        }
    }
}
