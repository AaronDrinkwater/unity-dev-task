﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AspectRatioContainer : MonoBehaviour
{
    const int resolutionX = 9;
    const int resolutionY = 16;

    void Start()
    {
        float screenRatio = Screen.width * 1f / Screen.height;
        float myRatio = resolutionX * 1f / resolutionY;

        if(screenRatio <= myRatio)
        {
            GetComponent<Camera>().rect = new Rect(0, (1f - screenRatio / myRatio) / 2f, 1, screenRatio / myRatio);
        }

        else if(screenRatio > myRatio)
        {
            GetComponent<Camera>().rect = new Rect((1f - myRatio / screenRatio) / 2f, 0, myRatio / screenRatio, 1);
        }
    }
    void Update()
    {
        
    }
}
